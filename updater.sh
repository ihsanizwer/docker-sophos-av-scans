#!/bin/bash
while read container
do
	echo "Updating sophos-av in ${container} ..."
	docker exec -i ${container} /opt/sophos-av/bin/savupdate
	echo "Complete updating for ${container}"
	done < ./containers.cfg