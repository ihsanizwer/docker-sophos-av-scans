#!/bin/bash
while read container
do
	echo "Installing in ${container}..."
	docker exec -ti ${container} /root/sav/setup/sophos-av/install.sh --automatic --acceptlicence --live-protection=False --update-free=True /opt/sophos-av
	echo ""
done < ./containers.cfg