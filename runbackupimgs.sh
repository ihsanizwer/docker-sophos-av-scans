#!/bin/bash
while read image
do
	echo "Running image, ${image} and mounting the volume"
	docker run --name=${image} -d -ti -v /root/sav-docker/volume:/root/sav ${image}
	if [ $? -eq 0 ];then
		echo -e "Container ${image} -\t\t up"
	else
		echo -e "Container ${image} -\t\t down"
	fi
done < ./containers.cfg