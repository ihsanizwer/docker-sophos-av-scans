#!/bin/bash
while read container
do
	touch ../volume/results/"${container}".txt
	echo -e "Scanning container: ${container}..."
	docker exec -i ${container} savscan / > ../volume/results/"${container}".txt
	echo "Compressing result file.."
	cd ../volume/results
	tar -cvzf "$container".tar.gz "${container}".txt
	check1=$?
	cd ../../docker-sophos-av-scans
	rm -rf ../volume/results/"${container}".txt
	check2=$?

	if ([ $check1 -eq 0 ] && [ $check2 -eq 0 ]);then
		echo "Scan results saved as ${container}.tar.gz"
	fi

	done < ./containers.cfg